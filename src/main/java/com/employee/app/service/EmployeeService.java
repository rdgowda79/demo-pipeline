package com.employee.app.service;

import java.util.List;

import com.employee.app.entity.Employee;
import com.employee.app.exception.EmployeeException;

public interface EmployeeService {
	Employee getEmployeeById(Integer id)throws EmployeeException;
	Employee addEmployee(Employee employee);
	List<Employee> getAllEmployees();
	Employee updateEmployee(Employee updateEmployee);
	Employee deleteEmployeeById(Integer employeeId);

}
