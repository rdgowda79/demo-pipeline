package com.employee.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.employee.app.exception.EmployeeException;
import com.employee.app.service.EmployeeService;

@SpringBootTest
class Demo1ApplicationTests {

	
	@Autowired
	private EmployeeService employeeService;
	
	@Test
	void getEnployeeByIdTest() throws EmployeeException {
		assertEquals("India", this.employeeService.getEmployeeById(1).getName());
	}
	@Test
	void getEnployeeByIdExceptionTest() throws EmployeeException {
		assertThrows(Exception.class, ()->this.employeeService.getEmployeeById(111));
	}

}
