package com.employee.app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import org.springframework.boot.test.context.SpringBootTest;

import com.employee.app.dao.EmployeeRepository;
import com.employee.app.entity.Employee;
import com.employee.app.exception.EmployeeException;

import com.employee.app.service.EmployeeServiceImpl;

@SpringBootTest

public class EmployeeServiceMockTests {

	@Mock
	EmployeeRepository employeeRepo;

	@InjectMocks
	EmployeeServiceImpl employeeService;

	@Test
	public void testEmployeeServiceGetByIdByMock() throws EmployeeException {

		//Stubbing
		Mockito.when(employeeRepo.findById(1)).thenReturn(Optional.of(new Employee(1, "Raghu", 100.0)));
		
		List<Employee> empList = new ArrayList<>();
		
		empList.add(new Employee(2, "name 2", 200.0));
		empList.add(new Employee(3, "name 3", 300.0));
		
		//Stubbing
				Mockito.when(employeeRepo.findAll()).thenReturn(empList);
				
		assertEquals("Raghu", employeeService.getEmployeeById(1).getName());
	}
	
	@Test
	public void testgetAllEmployeesByMock() throws EmployeeException {
		List<Employee> empList = new ArrayList<>();
		
		empList.add(new Employee(2, "name 2", 200.0));
		empList.add(new Employee(3, "name 3", 300.0));
		empList.add(new Employee(4, "name 4", 400.0));
		//Stubbing
				Mockito.when(employeeRepo.findAll()).thenReturn(empList);
				
		assertEquals(3, employeeService.getAllEmployees().size());
	}
}
